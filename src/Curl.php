<?php

declare(strict_types=1);

namespace Snugcomponents\Utils;

use CurlHandle;
use Nette\SmartObject;
use Snugcomponents\Utils\Curl\Request;

/** @phpstan-consistent-constructor */
class Curl
{
    use SmartObject;

    private CurlHandle $curl;

    /**
     * Cannot directly construct Curl, use Curl::create() instead
     * @see Curl::create()
     */
    private function __construct()
    {
        $this->curl = curl_init();
    }

    public static function create(): static
    {
        return new static();
    }

    private function setopt(int $option, mixed $value): static
    {
        curl_setopt($this->curl, $option, $value);
        return $this;
    }

    public function exec(Request $request): bool|string
    {
        foreach ($request->getOpts() as $key => $opt) {
            $this->setopt($key, $opt);
        }
        return curl_exec($this->curl);
    }

    public function getInfo(?int $option = null): mixed
    {
        return curl_getinfo($this->curl, $option);
    }

    public function close(): void
    {
        curl_close($this->curl);
    }

    public function __destruct()
    {
        $this->close();
    }
}
