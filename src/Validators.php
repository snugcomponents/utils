<?php

declare(strict_types=1);

namespace Snugcomponents\Utils;

use Exception;
use LifePHP\Utils\StaticClassTrait;
use Nette\Utils\Json;
use Nette\Utils\Strings;
use Snugcomponents\Utils\Curl\ClassicRequest;
use Snugcomponents\Utils\Helpers\Mobile\Format;
use stdClass;
use Tracy\Debugger;

class Validators
{
    use StaticClassTrait;

    // phpcs:disable
    private const string
        /**
         * ^                                            - beginning of string
         * (\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])         - number between 0 and 255 include, without leading zeros and dot  - (GROUP 2)
         * \.                                           - dot after the previous line
         * {3}                                          - previous two lines three times
         * (?2)                                         - after that again (GROUP 2) - number 0-255
         *                                              - next lines are optional, so they are in brackets and with ? at the end
         * \/                                           - slash dividing IP address and mask and
         * (\d)|([1-2]\d)|(3[0-2])|                     - number between 0 and 32 without leading zeros or
         * (0\.){3}0)|                                  - 0.0.0.0 mask or
         *
         * (128|192|224|240|248|252)                    - numbers which can appear in mask end (GROUP 13)
         * ((128|192|224|240|248|252)|254|255)          - numbers which can appear in mask part (GROUP 12)
         * (255\.)                                      - leading mask part (GROUP 16)
         * (\.0)                                        - ending mask part (GROUP 14)
         *
         * (((128|192|224|240|248|252)|255)(\.0){3})|   - (GROUP 12).0.0.0 mask or
         * ((255\.)(?12)(?14){2})|                      - 255.(GROUP 12).0.0 mask or
         * ((?16){2}(?12)(?14))|                        - 255.255.(GROUP 12).0 mask or
         * ((?16){3}(?13))                              - 255.255.255.(GROUP 13)
         * ?                                            - optional mask
         * $                                            - end of string
         */
        REGEXP_IPv4 = '~^((\d|[1-9]\d|1\d{2}|2[0-4]\d|25[0-5])\.){3}(?2)((\/((\d)|([1-2]\d)|(3[0-2])|((0\.){3}0)|(((128|192|224|240|248|252)|254|255)(\.0){3})|((255\.)(?12)(?14){2})|((?16){2}(?12)(?14))|((?16){3}(?13)))))?$~';
    // phpcs:enable

    public static function isPersonalIdentificationNumberCZ(string $pin): bool
    {
        $numberOfSlashes = substr_count($pin, '/');
        if (
            ($numberOfSlashes === 1
            && strpos($pin, '/') !== 6)
            || $numberOfSlashes > 1
        ) {
            return false;
        }

        $pin = str_replace('/', '', $pin);
        $length = Strings::length($pin);
        if (
            $length !== 9
            && $length !== 10
            || !($dateOfBirth = Convertors::getDateOfBirthFromPersonalIdentificationNumberCZ($pin))
        ) {
            return false;
        }

        if ($length === 9) {
            return true;
        }

        $pin = (int)$pin;

        if (($pin % 11) === 0) {
            return true;
        }

        $year = $dateOfBirth->format('Y');

        if ($year < 1986 && (floor($pin / 10) % 11) === 10) {
            return ($pin % 10) === 0;
        }

        return false;
    }

    public static function isIPv4Valid(string $address): bool
    {
        return (bool) preg_match(self::REGEXP_IPv4, Strings::trim($address));
    }

    /**
     * TODO: validate also CIDR/MASK notation
     */
    public static function isIPv6Valid(string $address): bool
    {
        return strlen((string) inet_pton(Strings::trim($address))) === 16;
    }

    public static function isIpAddressValid(string $address): bool
    {
        return self::isIPv4Valid($address) || self::isIPv6Valid($address);
    }

    /**
     * @throws Exception
     */
    public static function isMobileValid(
        string $mobile,
        string $continent = 'Europe',
        string $country = 'CZ',
    ): bool {
        return Format::isMobileValid(
            $mobile,
            $continent,
            $country,
        );
    }

    public static function isReCaptchaValid(
        string $secretKey,
        string $challengeToken,
    ): bool {
        $request = ClassicRequest::create('https://www.google.com/recaptcha/api/siteverify');

        $request->setData(key: 'secret', value: $secretKey);
        $request->setData(key: 'response', value: $challengeToken);

        $response = Curl::create()
            ->exec($request);

        if (is_bool($response)) {
            return false;
        }

        $response = Json::decode($response);
        assert($response instanceof stdClass);

        if (!$response->success) {
            Debugger::log($response);
        }

        return $response->success;
    }

    /**
     * Latin 1 format: https://cs.wikipedia.org/wiki/ISO_8859-1
     */
    public static function containsOnlyLatin1Chars(string $utf8String): bool
    {
        $utf8String = mb_convert_encoding($utf8String, 'UTF-8', 'UTF-8'); // just to be sure
        $utf8Chars = mb_str_split($utf8String, 1, 'UTF-8');

        foreach ($utf8Chars as $utf8Char) {
            $ord = mb_ord($utf8Char, 'UTF-8');
            if ($ord > 0xFF) { // uses more than 1 Byte
                return false;
            }
        }

        return true;
    }
}
