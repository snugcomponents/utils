<?php

declare(strict_types=1);

namespace Snugcomponents\Utils;

use Exception;
use LifePHP\Utils\StaticClassTrait;
use Nette\InvalidArgumentException;
use Nette\Utils\DateTime;
use Nette\Utils\Json;
use Nette\Utils\JsonException;
use Nette\Utils\Strings;
use Normalizer;
use Snugcomponents\Utils\Helpers\Mobile\Format;
use stdClass;

class Convertors
{
    use StaticClassTrait;

    public const string
        SEX_MAN = 'man',
        SEX_WOMAN = 'woman',

        ADDRESS_LAT = 'lat',
        ADDRESS_LNG = 'lng';

    /** @deprecated deprecated since version 1.1.0 use Convertors::SEX_MAN */
    public const string GENDER_MAN = self::SEX_MAN;

    /** @deprecated deprecated since version 1.1.0 use Convertors::SEX_WOMAN */
    public const string GENDER_WOMAN = self::SEX_WOMAN;

    private const array
        ADDRESS_DEFAULT_LAT_LNG = [
            self::ADDRESS_LAT => null,
            self::ADDRESS_LNG => null,
        ];

    /**
     * @var array<string, array{lat: float, lng: float}|array{lat: null, lng: null}>
     */
    private static array $googleapisFetchedAddresses = [];

    /**
     * This function counts on that $pin parameter is valid.
     * For Validation please use 'Snugcomponents\Utils\Validators::isPersonalIdentificationNumberCZ' validator.
     *
     * @param string $pin
     * @return DateTime|null
     */
    public static function getDateOfBirthFromPersonalIdentificationNumberCZ(string $pin): ?DateTime
    {
        $length = Strings::length($pin);

        if ($length < 9) {
            return null;
        }

        $year = (int) ($pin[0] . $pin[1]);
        $month = (int) ($pin[2] . $pin[3]);
        $day = (int) ($pin[4] . $pin[5]);

        $year += 1900;
        $month = $month > 50 ? $month - 50 : $month;    // Woman's
        $month = $month > 20 ? $month - 20 : $month;    // Out of PIN that day

        if ($length === 10 && $year < 1954) {
            $year += 100;
        }

        try {
            return DateTime::fromParts($year, $month, $day);
        } catch (InvalidArgumentException) {
            return null;
        }
    }

    /**
     * This function counts on that $pin parameter is valid.
     * For Validation please use 'Snugcomponents\Utils\Validators::isPersonalIdentificationNumberCZ' validator.
     *
     * @param string $pin
     * @return string
     */
    public static function getSexFromPersonalIdentificationNumberCZ(string $pin): string
    {
        return $pin && $pin[2] >= 5 ? self::SEX_WOMAN : self::SEX_MAN;
    }

    /**
     * @deprecated deprecated since version 1.1.0 use Convertors::getSexFromPersonalIdentificationNumberCZ()
     */
    public static function getGenderFromPersonalIdentificationNumberCZ(string $pin): string
    {
        return self::getSexFromPersonalIdentificationNumberCZ($pin);
    }

    /**
     * @return array{lat: float, lng: float}|array{lat: null, lng: null}
     * @throws JsonException
     */
    public static function getGPSFromAddressByGoogleapis(string $address, string $googleapisServerKey): array
    {
        if (!$address) {
            return self::ADDRESS_DEFAULT_LAT_LNG;
        }

        if (!isset(self::$googleapisFetchedAddresses[$address])) {
            $response = file_get_contents(
                sprintf(
                    'https://maps.googleapis.com/maps/api/geocode/json?address=%s&key=%s',
                    urlencode($address),
                    $googleapisServerKey,
                ),
            );

            if (empty($response)) {
                return self::ADDRESS_DEFAULT_LAT_LNG;
            }

            $addr = Json::decode($response);
            assert($addr instanceof stdClass);
            $results = $addr->results ?? [];

            if (empty($results)) {
                return self::ADDRESS_DEFAULT_LAT_LNG;
            }

            $firstKey = array_key_first($results);
            $result = $results[$firstKey];
            assert($result instanceof stdClass);

            $result = $result
                ->geometry
                ?->location;

            self::$googleapisFetchedAddresses[$address] // @phpstan-ignore-line
                = (array) ($result ?? self::ADDRESS_DEFAULT_LAT_LNG);
        }

        return self::$googleapisFetchedAddresses[$address]; // @phpstan-ignore-line
    }

    /**
     * @throws Exception
     */
    public static function convertMobileToRightFormat(
        string $mobile,
        string $continent = 'Europe',
        string $country = 'CZ',
    ): string {
        return Format::formatFilter(
            $mobile,
            $continent,
            $country,
        );
    }

    /**
     * Latin 1 format: https://cs.wikipedia.org/wiki/ISO_8859-1
     */
    public static function convertToLatin1(string $utf8String): string
    {
        $utf8String = mb_convert_encoding($utf8String, 'UTF-8', 'UTF-8'); // just to be sure
        $utf8Chars = mb_str_split($utf8String, 1, 'UTF-8');
        $retVal = '';

        foreach ($utf8Chars as $utf8Char) {
            $ord = mb_ord($utf8Char, 'UTF-8');

            if ($ord <= 0xFF) { // uses more than 1 Byte
                $retVal .= $utf8Char;
                continue;
            }

            $chars = Normalizer::normalize($utf8Char, Normalizer::FORM_D);

            if ($chars === false) {
                continue;
            }

            $retVal .= mb_substr($chars, 0, 1, 'UTF-8');
        }

        return mb_convert_encoding($retVal, 'ISO-8859-1', 'UTF-8');
    }
}
