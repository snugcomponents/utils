<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile;

use Exception;
use Nette\SmartObject;
use Nette\Utils\Strings;
use Snugcomponents\Utils\Helpers\Mobile\Continent\Europe\{
    CountryAD,
    CountryAL,
    CountryAT,
    CountryBA,
    CountryBE,
    CountryBG,
    CountryBY,
    CountryCH,
    CountryCY,
    CountryCZ,
    CountryDE,
    CountryDK,
    CountryEE,
    CountryES,
    CountryFI,
    CountryFO,
    CountryFR,
    CountryGB,
    CountryGE,
    CountryGI,
    CountryGR,
    CountryHR,
    CountryHU,
    CountryIE,
    CountryIS,
    CountryIT,
    CountryLT,
    CountryLU,
    CountryLV,
    CountryMC,
    CountryMK,
    CountryMT,
    CountryNL,
    CountryNO,
    CountryPL,
    CountryPT,
    CountryRO,
    CountryRU,
    CountrySE,
    CountrySI,
    CountrySK,
    CountrySM,
    CountryTR,
    CountryUA,
    CountryVA,
};

abstract class Format
{
    use SmartObject;

    /**
     * @var array<string, array<string, class-string<Format>>>
     */
    private static array $formats = [
        'Europe' => [
            'AL' => CountryAL::class,
            'AD' => CountryAD::class,
            'AT' => CountryAT::class,
            'BY' => CountryBY::class,
            'BE' => CountryBE::class,
            'BA' => CountryBA::class,
            'BG' => CountryBG::class,
            'CH' => CountryCH::class,
            'CY' => CountryCY::class,
            'CZ' => CountryCZ::class,
            'DE' => CountryDE::class,
            'DK' => CountryDK::class,
            'EE' => CountryEE::class,
            'ES' => CountryES::class,
            'FO' => CountryFO::class,
            'FI' => CountryFI::class,
            'FR' => CountryFR::class,
            'GB' => CountryGB::class,
            'GE' => CountryGE::class,
            'GI' => CountryGI::class,
            'GR' => CountryGR::class,
            'HU' => CountryHU::class,
            'HR' => CountryHR::class,
            'IE' => CountryIE::class,
            'IS' => CountryIS::class,
            'IT' => CountryIT::class,
            'LT' => CountryLT::class,
            'LU' => CountryLU::class,
            'LV' => CountryLV::class,
            'MC' => CountryMC::class,
            'MK' => CountryMK::class,
            'MT' => CountryMT::class,
            'NO' => CountryNO::class,
            'NL' => CountryNL::class,
            'PL' => CountryPL::class,
            'PT' => CountryPT::class,
            'RO' => CountryRO::class,
            'RU' => CountryRU::class,
            'SE' => CountrySE::class,
            'SI' => CountrySI::class,
            'SK' => CountrySK::class,
            'SM' => CountrySM::class,
            'TR' => CountryTR::class,
            'UA' => CountryUA::class,
            'VA' => CountryVA::class,
        ],
    ];

    protected function __construct(
        public readonly string $prefix,
        public readonly string $number,
        public readonly string $format,
    ) {
    }

    /**
     * @throws Exception
     */
    public static function isMobileValid(
        string $mobile,
        string $continent = 'Europe',
        string $country = 'CZ',
    ): bool {
        return self::createFormat(
            $continent,
            $country,
        )->matchMobile($mobile);
    }

    /**
     * @throws Exception
     */
    public static function formatFilter(
        string $mobile,
        string $continent = 'Europe',
        string $country = 'CZ',
        bool $suppressMissingCountryError = true,
    ): string {
        try {
            return self::createFormat(
                $continent,
                $country,
            )->processFilter($mobile);
        } catch (Exception $exception) {
            if (!$suppressMissingCountryError) {
                throw $exception;
            }
        }
        return $mobile;
    }

    /**
     * @throws Exception
     */
    public static function createFormat(
        string $continent = 'Europe',
        string $country = 'CZ',
    ): Format {
        $concreteFormat = self::$formats[$continent][$country] ?? null;

        if (!$concreteFormat) {
            throw new Exception(
                'Selected country does not exists on our list. Please contact support or contribute.'
            );
        }

        return new $concreteFormat();
    }

    public function processFilter(
        string $mobile,
    ): string {
        $withoutPrefix = $this->removePrefix($mobile);
        $format = $this->format;
        $retVal = Strings::match($format, $this->prefix)[0];    // @phpstan-ignore-line
        $format = (string) Strings::after($format, $retVal);
        $j = 0;

        for ($i = 0; $i < Strings::length($format); $i++) {
            if ($format[$i] != 'x') {
                $retVal .= $format[$i];
                continue;
            }
            $retVal .= $withoutPrefix[$j++];
        }

        return $retVal;
    }

    public function matchMobile(
        string $mobile,
    ): bool {
        return Strings::match(
            $this->removePrefix($mobile),
            $this->number,
        ) !== null;
    }

    private function removePrefix(
        string $mobile,
    ): string {
        $value = Strings::replace($mobile, '/\s|-/');

        $prefix = Strings::match($value, $this->prefix);

        if ($prefix !== null) {
            $value = (string) Strings::after($value, $prefix[0]);
        }

        return $value;
    }
}
