<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryNL extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+31/',
            number: '/^\d{9}$/',
            format: '+31 x xxxx xxxx',
        );
    }
}
