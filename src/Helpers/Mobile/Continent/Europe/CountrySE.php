<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountrySE extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+46/',
            number: '/^\d{9}$/',
            format: '+46 xxx xxx xxx',
        );
    }
}
