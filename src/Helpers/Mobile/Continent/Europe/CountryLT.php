<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryLT extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+370/',
            number: '/^\d{8}$/',
            format: '+370 x xxx xxxx',
        );
    }
}
