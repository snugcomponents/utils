<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryCH extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+41/',
            number: '/^7[5-9]\d{7}$/',
            format: '+41 7x xxx xx xx'
        );
    }
}
