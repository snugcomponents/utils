<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryIE extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+353/',
            number: '/^\d{9}$/',
            format: '+353 xx xxx xxxx',
        );
    }
}
