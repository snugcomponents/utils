<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryAT extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+43/',
            number: '/^\d*$/',
            format: '+43 ',
        );
    }
}
