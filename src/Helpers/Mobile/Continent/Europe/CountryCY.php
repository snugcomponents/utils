<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryCY extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+357/',
            number: '/^9\d{7}$/',
            format: '+357 9x xxxxxx',
        );
    }
}
