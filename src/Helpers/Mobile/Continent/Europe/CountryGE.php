<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryGE extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+995/',
            number: '/^\d{9}$/',
            format: '+995 - xxx - xxx xxx',
        );
    }
}
