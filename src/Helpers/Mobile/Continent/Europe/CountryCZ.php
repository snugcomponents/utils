<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryCZ extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+420/',
            number: '/^\d{9}$/',
            format: '+420 xxx xxx xxx',
        );
    }
}
