<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryFI extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+358/',
            number: '/^9\d{6}$/',
            format: '+358 9 xxx xxx',
        );
    }
}
