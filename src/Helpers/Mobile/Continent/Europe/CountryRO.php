<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryRO extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+40/',
            number: '/^\d{9}$/',
            format: '+40 xxx xxx xxx',
        );
    }
}
