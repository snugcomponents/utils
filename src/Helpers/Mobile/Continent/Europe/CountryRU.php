<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryRU extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+7/',
            number: '/^\d{10}$/',
            format: '+7 xxx xxx xxxx',
        );
    }
}
