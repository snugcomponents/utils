<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryBG extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+359/',
            number: '/^2\d{7}$/',
            format: '+359 2 xxx xxxx',
        );
    }
}
