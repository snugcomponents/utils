<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryHU extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+36/',
            number: '/^\d{8}$/',
            format: '+36 x xxx xxxx',
        );
    }
}
