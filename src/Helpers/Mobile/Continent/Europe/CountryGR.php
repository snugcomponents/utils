<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryGR extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+30/',
            number: '/^69\d{8}$/',
            format: '+30 6 9x xxxxxxx',
        );
    }
}
