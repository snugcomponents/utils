<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryUA extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+380/',
            number: '/^\d{9}$/',
            format: '+380 xx xxx-xx-xx',
        );
    }
}
