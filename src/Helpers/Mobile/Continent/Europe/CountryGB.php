<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryGB extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+44/',
            number: '/^\d{10}$/',
            format: '+44 7777666666',
        );
    }
}
