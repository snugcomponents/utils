<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryDK extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+45/',
            number: '/^\d{8}$/',
            format: '+45 xx-xx-xx-xx',
        );
    }
}
