<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryBA extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+387/',
            number: '/^33\d{6}$/',
            format: '+387 33 xxx xxx',
        );
    }
}
