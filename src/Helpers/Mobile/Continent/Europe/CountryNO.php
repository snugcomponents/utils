<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryNO extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+47/',
            number: '/^59\d{6}$/',
            format: '+47 59x xx xxx',
        );
    }
}
