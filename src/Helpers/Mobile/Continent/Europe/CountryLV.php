<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryLV extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+371/',
            number: '/^2\d{7}$/',
            format: '+371 2x xxx xxx',
        );
    }
}
