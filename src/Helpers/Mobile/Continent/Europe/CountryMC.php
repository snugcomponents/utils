<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryMC extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+377/',
            number: '/^6\d{8}$/',
            format: '+377 6 xx xx xx xx',
        );
    }
}
