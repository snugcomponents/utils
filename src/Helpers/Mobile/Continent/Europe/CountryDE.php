<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryDE extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+49/',
            number: '/^01[5-7]\d{9}$/',
            format: '+49 01xxx-xxxxxxx',
        );
    }
}
