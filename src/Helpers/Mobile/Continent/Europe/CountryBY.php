<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryBY extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+375/',
            number: '/^\d{9}$/',
            format: '+375 xx xxx xxxx',
        );
    }
}
