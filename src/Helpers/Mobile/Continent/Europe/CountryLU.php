<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryLU extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+352/',
            number: '/^6\d1\d{6}$/',
            format: '+352 6x1 xxx xxx',
        );
    }
}
