<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryFO extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+298/',
            number: '/^\d{6}$/',
            format: '+298 xxxxxx',
        );
    }
}
