<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryEE extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+372/',
            number: '/^\d{8}$/',
            format: '+372 xxxx xxxx',
        );
    }
}
