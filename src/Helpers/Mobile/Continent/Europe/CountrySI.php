<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountrySI extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+386/',
            number: '/^1\d{7}$/',
            format: '+386 1 xxx xx xx',
        );
    }
}
