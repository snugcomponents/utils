<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryIT extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+39/',
            number: '/^\d{10}$/',
            format: '+39 xxx xxxxxxx',
        );
    }
}
