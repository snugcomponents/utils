<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryAL extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+355/',
            number: '/^4\d{7}$/',
            format: '+355 4 xxx xxxx',
        );
    }
}
