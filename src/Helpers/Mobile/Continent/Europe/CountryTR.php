<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryTR extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+90/',
            number: '/^\d{10}$/',
            format: '+90 xxx xxx xxxx',
        );
    }
}
