<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryHR extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+385/',
            number: '/^\d{9}$/',
            format: '+385 xx xxx xxxx',
        );
    }
}
