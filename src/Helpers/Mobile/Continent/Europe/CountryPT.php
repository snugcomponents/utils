<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryPT extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+351/',
            number: '/^9\d{8}$/',
            format: '+351 9x xxx xxxx',
        );
    }
}
