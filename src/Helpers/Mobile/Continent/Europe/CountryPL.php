<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryPL extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+48/',
            number: '/^\d{9}$/',
            format: '+48 xxx xxx xxx',
        );
    }
}
