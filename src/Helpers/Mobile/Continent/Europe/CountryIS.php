<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Helpers\Mobile\Continent\Europe;

use Snugcomponents\Utils\Helpers\Mobile\Format;

class CountryIS extends Format
{
    public function __construct()
    {
        parent::__construct(
            prefix: '/^\+354/',
            number: '/^\d{7}$/',
            format: '+354 xxx xxxx',
        );
    }
}
