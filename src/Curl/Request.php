<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Curl;

use Exception;
use Nette\Http\IRequest;
use Nette\SmartObject;
use Snugcomponents\Utils\Curl;

/** @phpstan-consistent-constructor */
abstract class Request
{
    use SmartObject;

    protected bool $initializedContentTypeHeader = false;
    /**
     * @var array<int, mixed>
     */
    private array $opts = [];
    /**
     * @var array<string, string>
     */
    private array $headers = [];

    protected function __construct(string $url = '')
    {
        $this->makePost();
        $this->setReturnTransfer(true);
        if ($url) {
            $this->setUrl($url);
        }
    }

    public static function create(string $url = ''): static
    {
        return new static($url);
    }

    protected function addHeader(
        string $name,
        string $value,
        bool $endWithSemicolon = true,
    ): static {
        if ($name === 'Content-Type') {
            if ($this->initializedContentTypeHeader) {
                throw new Exception('You cannot redeclare Content-Type header. Use another Request or make your own.');
            }
            $this->initializedContentTypeHeader = true;
        }

        $this->headers[$name] = $name . ': ' . $value . ($endWithSemicolon ? ';' : '');
        return $this->setopt(CURLOPT_HTTPHEADER, array_values($this->headers));
    }

    protected function setopt(int $option, mixed $value): static
    {
        $this->opts[$option] = $value;
        return $this;
    }

    protected function getUrl(): string
    {
        $retVal = $this->opts[CURLOPT_URL];
        assert(is_string($retVal));
        return $retVal;
    }

    /**
     * @param array<string> $headers
     * @param bool $endWithSemicolon
     * @return $this
     * @throws Exception
     */
    public function setHeaders(array $headers, bool $endWithSemicolon = false): static
    {
        foreach ($headers as $header) {
            [
                $name,
                $value,
            ] = explode(': ', $header, 2);
            $this->addHeader(name: $name, value: $value, endWithSemicolon: $endWithSemicolon);
        }

        return $this;
    }

    public function setEncoding(
        string $encoding,
    ): static {
        return $this->setopt(CURLOPT_ENCODING, $encoding);
    }

    public function followLocation(): static
    {
        return $this->setopt(CURLOPT_FOLLOWLOCATION, true);
    }

    public function disableSslVerifypeer(): static
    {
        return $this->setopt(CURLOPT_FOLLOWLOCATION, false);
    }

    public function setUrl(
        string $url,
    ): static {
        return $this->setopt(CURLOPT_URL, $url);
    }

    public function setUserPWD(
        string $username,
        string $password,
    ): static {
        return $this->setopt(CURLOPT_USERPWD, $username . ':' . $password);
    }

    public function setBearerToken(
        string $token,
    ): static {
        return $this->addHeader('Authorization', sprintf('Bearer %s', $token), false);
    }

    /**
     * CURLOPT_RETURNTRANSFER is set to true in constructor
     */
    public function setReturnTransfer(bool $returnTransfer): static
    {
        return $this->setopt(CURLOPT_RETURNTRANSFER, $returnTransfer);
    }

    public function includeCurlinfoHeaderInInfo(): static
    {
        return $this->setopt(CURLINFO_HEADER_OUT, true);
    }


    public function makePost(): static
    {
        return $this->setopt(CURLOPT_CUSTOMREQUEST, IRequest::Post);
    }

    public function makeGet(): static
    {
        return $this->setopt(CURLOPT_CUSTOMREQUEST, IRequest::Get);
    }

    public function makePut(): static
    {
        return $this->setopt(CURLOPT_CUSTOMREQUEST, IRequest::Put);
    }

    public function makeDelete(): static
    {
        return $this->setopt(CURLOPT_CUSTOMREQUEST, IRequest::Delete);
    }

    public function makeHead(): static
    {
        return $this->setopt(CURLOPT_CUSTOMREQUEST, IRequest::Head);
    }

    public function makeOptions(): static
    {
        return $this->setopt(CURLOPT_CUSTOMREQUEST, IRequest::Options);
    }

    public function makePatch(): static
    {
        return $this->setopt(CURLOPT_CUSTOMREQUEST, IRequest::Patch);
    }

    /**
     * @return array<int, mixed>
     * @throws Exception
     * @internal
     */
    final public function getOpts(): array
    {
        if (!$this->isCalledFromCurl()) {
            throw new Exception(
                sprintf(
                    'Sorry but calling `%s::getOpts` function is allowed only from `%s`. You can extend it.',
                    Request::class,
                    Curl::class,
                ),
            );
        }
        return $this->opts;
    }

    private function isCalledFromCurl(): bool
    {
        //get the trace
        $trace = debug_backtrace();

        foreach ($trace as $call) {
            if (!isset($call['class'])) { // it is called from global context (outside of class)
                return false;
            }

            if (Request::class !== $call['class']) { // is it a different class than Request::class
                return $call['class'] === Curl::class;
            }
        }

        return false;
    }
}
