<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Curl;

use Nette\Utils\Json;
use Nette\Utils\JsonException;

class JsonRequest extends Request
{
    protected function __construct(string $url = '')
    {
        $this->addHeader(
            'Content-Type',
            'application/json',
        );
        parent::__construct($url);
    }

    /**
     * @param string|iterable<int, mixed> $body
     * @throws JsonException
     */
    public function setBody(
        string|iterable $body,
    ): static {
        if (is_string($body)) {
            $isJsonValid = json_validate(
                json: $body,
                depth: (1 << 31) - 1, // 32bit signed-int max
            );

            if (!$isJsonValid) {
                throw new JsonException(json_last_error_msg(), json_last_error());
            }
        } else {
            $body = Json::encode($body);
        }

        return $this->setopt(CURLOPT_POSTFIELDS, $body);
    }
}
