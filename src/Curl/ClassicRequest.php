<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Curl;

use Nette\Utils\Strings;

class ClassicRequest extends Request
{
    /**
     * @var array<int|string, mixed>
     */
    private array $postData = [];

    protected function __construct(string $url = '')
    {
        $this->initializedContentTypeHeader = true;
        parent::__construct($url);
    }

    public function setData(
        string $key,
        mixed $value,
    ): static {
        $this->postData[$key] = $value;
        return $this->setopt(CURLOPT_POSTFIELDS, $this->postData);
    }

    /**
     * @param array<int|string, mixed> $data
     */
    public function setDataArray(
        array $data,
    ): static {
        $this->postData = array_merge($this->postData, $data);
        return $this->setopt(CURLOPT_POSTFIELDS, $this->postData);
    }

    /**
     * @param array<string, scalar> $parameters
     */
    public function setQueryParameters(array $parameters): static
    {
        $data = http_build_query($parameters);
        return $this->setUrl(Strings::before($this->getUrl() . '?', '?') . '?' . $data);
    }
}
