<?php

declare(strict_types=1);

namespace Snugcomponents\Utils\Curl;

use CURLFile;

class FileRequest extends ClassicRequest
{
    private int $curlFilesCount = 0;

    /**
     * Create a CURLFile object and add it to send body
     *
     * @link https://secure.php.net/manual/en/curlfile.construct.php
     * @param string $filename <p>Path to the file which will be uploaded.</p>
     * @param string $mimeType [optional] <p>Mimetype of the file.</p>
     * @param string $postedFilename [optional] <p>Name of the file.</p>
     */
    public function addFile(
        string $filename,
        string $mimeType = '',
        string $postedFilename = '',
    ): static {
        return $this->setData('upload_file_' . $this->curlFilesCount++, new CURLFile(
            $filename,
            $mimeType,
            $postedFilename,
        ));
    }
}
