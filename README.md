## Installation

To install the latest version of `snugcomponents/utils` use [Composer](https://getcomposer.org).

```bash
composer require snugcomponents/utils
```

## Documentation

For details on how to use this package, check out our [documentation](.docs).

## Versions

| State  | Version  | Branch     | Nette | PHP     |
|--------|----------|------------|-------|---------|
| stable | `^2.3.1` | `Nette4.0` | `4.0` | `>=8.3` |

## Development

This package is currently maintaining by these authors:

<a href="https://gitlab.com/tondajehlar">
  <img alt="TonnyJe" width="80" height="80" src="https://avatars2.githubusercontent.com/u/9120518?v=3&s=80">
</a>

<a href="https://gitlab.com/miellap">
  <img alt="Miellap" width="80" height="80" src="https://gitlab.com/uploads/-/system/user/avatar/14030435/avatar.png?width=400">
</a>

-----

## Conclusion
This package requires PHP8.3, Nette4.0, and it is property of SnugDesign © 2024
